package snakestatevisualiser;

/**
 *
 * @author Jeremy
 */
import java.awt.Point;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class Snake {

    boolean alive;
    boolean invisible;
    int length;
    int kills;
    String[] coords;
    Point head;
    int number;
    String initialisationString;
    boolean power = false;

    enum Direction {

        Left, Right, Up, Down
    }

    public Snake(String initialisationString, int SnakeNumber) {
        this.initialisationString = initialisationString;
        String[] initialisationParameters = initialisationString.split(" ");
        alive = initialisationParameters[0].equals("alive") || initialisationParameters[0].equals("invisible");
        if (initialisationParameters[0].equals("invisible")) {
            invisible = true;
        }
        number = SnakeNumber;
        if (alive) {
            length = Integer.parseInt(initialisationParameters[1]);
            kills = Integer.parseInt(initialisationParameters[2]);

            
            int start = 3;
            if (invisible) {
                int timeSinceInvisible = Integer.parseInt(initialisationParameters[3]);
                start = 5;
            }
            coords = new String[initialisationParameters.length - start];
            if (initialisationParameters.length <= start) { //completely invisible snake
                coords = null;
                head = null;
                return;
            }
            String[] headCoords = initialisationParameters[start].split(",");
            head = new Point(Integer.parseInt(headCoords[0]), Integer.parseInt(headCoords[1]));
            for (int i = start; i < initialisationParameters.length; i++) {
                coords[i - start] = initialisationParameters[i];
            }
        }
    }

    public Polygon getSnakePolygon() {
        if (coords == null) {
            return null;
        }

        ArrayList<Point> PolygonLeft = new ArrayList<Point>();
        ArrayList<Point> PolygonRight = new ArrayList<Point>();

        //int p2Pos = 1; //case where there is only 1 segment.
        //if (coords.length == 1) {
        //    p2Pos = 0;
        //}
        String[] p1Coords = coords[0].split(",");
        String[] p2Coords = coords[1].split(",");

        Point p1 = new Point(Integer.parseInt(p1Coords[0]), Integer.parseInt(p1Coords[1]));
        Point p2 = new Point(Integer.parseInt(p2Coords[0]), Integer.parseInt(p2Coords[1]));

        Direction currentDirection = getAbsoluteDirection(p1, p2);

        Bounds b = new Bounds(p1);

        if (currentDirection == Direction.Right) {
            PolygonLeft.add(b.topLeft);
            PolygonRight.add(b.bottomLeft);
        } else if (currentDirection == Direction.Left) {
            PolygonLeft.add(b.bottomRight);
            PolygonRight.add(b.topRight);
        } else if (currentDirection == Direction.Up) {
            PolygonLeft.add(b.bottomLeft);
            PolygonRight.add(b.bottomRight);
        } else if (currentDirection == Direction.Down) {
            PolygonLeft.add(b.topRight);
            PolygonRight.add(b.topLeft);
        }

        for (int i = 1; i < coords.length - 1; i++) {
            String[] point1Coords = coords[i].split(",");
            String[] point2Coords = coords[i + 1].split(",");
            Point point1 = new Point(Integer.parseInt(point1Coords[0]), Integer.parseInt(point1Coords[1]));
            Point point2 = new Point(Integer.parseInt(point2Coords[0]), Integer.parseInt(point2Coords[1]));

            Direction previousDirection = currentDirection;

            currentDirection = getAbsoluteDirection(point1, point2);

            Direction directionTurned = getDirectionTurned(previousDirection, currentDirection);

            b = new Bounds(point1);

            if (directionTurned == Direction.Right && previousDirection == Direction.Right) {
                PolygonLeft.add(b.topRight);
                PolygonRight.add(b.bottomLeft);
            } else if (directionTurned == Direction.Right && previousDirection == Direction.Down) {
                PolygonLeft.add(b.bottomRight);
                PolygonRight.add(b.topLeft);
            } else if (directionTurned == Direction.Right && previousDirection == Direction.Left) {
                PolygonLeft.add(b.bottomLeft);
                PolygonRight.add(b.topRight);
            } else if (directionTurned == Direction.Right && previousDirection == Direction.Up) {
                PolygonLeft.add(b.topLeft);
                PolygonRight.add(b.bottomRight);
            } else if (directionTurned == Direction.Left && previousDirection == Direction.Right) {
                PolygonLeft.add(b.topLeft);
                PolygonRight.add(b.bottomRight);
            } else if (directionTurned == Direction.Left && previousDirection == Direction.Up) {
                PolygonLeft.add(b.bottomLeft);
                PolygonRight.add(b.topRight);
            } else if (directionTurned == Direction.Left && previousDirection == Direction.Left) {
                PolygonLeft.add(b.bottomRight);
                PolygonRight.add(b.topLeft);
            } else if (directionTurned == Direction.Left && previousDirection == Direction.Down) {
                PolygonLeft.add(b.topRight);
                PolygonRight.add(b.bottomLeft);
            }
        }

        String[] lastPointCoords = coords[coords.length - 1].split(",");
        Point lastPoint = new Point(Integer.parseInt(lastPointCoords[0]), Integer.parseInt(lastPointCoords[1]));

        b = new Bounds(lastPoint);

        if (currentDirection == Direction.Left) {
            PolygonLeft.add(b.bottomLeft);
            PolygonRight.add(b.topLeft);
        } else if (currentDirection == Direction.Right) {
            PolygonLeft.add(b.topRight);
            PolygonRight.add(b.bottomRight);
        } else if (currentDirection == Direction.Up) {
            PolygonLeft.add(b.topLeft);
            PolygonRight.add(b.topRight);
        } else if (currentDirection == Direction.Down) {
            PolygonLeft.add(b.bottomRight);
            PolygonRight.add(b.bottomLeft);
        }

        double[] points = new double[(PolygonLeft.size() + PolygonRight.size()) * 2];

        int index = 0;

        for (Point p : PolygonLeft) {
            points[index++] = p.x;
            points[index++] = p.y;
        }

        for (int i = PolygonRight.size() - 1; i >= 0; i--) {
            Point p = PolygonRight.get(i);
            points[index++] = p.x;
            points[index++] = p.y;
        }

        Polygon p = new Polygon(points);
        if (invisible) {
            p.getStyleClass().add("supersnake");
        }
        return p;
    }

    private Point getPointModifier(Point point1, Point point2, Point pointModifier) {

        if (point1.y == point2.y) {
            if (point1.x < point2.x) {
                return new Point(pointModifier.x + 1, pointModifier.y); // snake turned right
            } else {
                return new Point(pointModifier.x - 1, pointModifier.y); // snake turned left
            }
        } else {
            if (point1.y < point2.y) {
                return new Point(pointModifier.x, pointModifier.y + 1); // snake turned down
            } else {
                return new Point(pointModifier.x, pointModifier.y - 1);// snake turned up
            }
        }
    }

    private Direction getAbsoluteDirection(Point point1, Point point2) {
        if (point1.y == point2.y) {
            if (point1.x < point2.x) {
                return Direction.Right;
            } else {
                return Direction.Left;
            }
        } else {
            if (point1.y < point2.y) {
                return Direction.Down;
            } else {
                return Direction.Up;
            }
        }
    }

    private Direction getDirectionTurned(Direction previousDirection, Direction currentDirection) {
        if (previousDirection == Direction.Up && currentDirection == Direction.Right
                || previousDirection == Direction.Right && currentDirection == Direction.Down
                || previousDirection == Direction.Down && currentDirection == Direction.Left
                || previousDirection == Direction.Left && currentDirection == Direction.Up) {
            return Direction.Right;
        }
        return Direction.Left;
    }

    private Point getModifiedPoint(Point point2, Point pointModifier) {
        return new Point((point2.x + pointModifier.x) * 10, (point2.y + pointModifier.y) * 10);
    }

    private Point getModifiedPoint(Point point1) {
        return new Point(point1.x * 10, point1.y * 10);
    }
}

class Bounds {

    Point topLeft;
    Point topRight;
    Point bottomLeft;
    Point bottomRight;

    public Bounds(Point p) {
        this(p.x, p.y);
    }

    public Bounds(int x, int y) {
        int top = y * 10;
        int bottom = (y + 1) * 10 - 1;
        int left = x * 10;
        int right = (x + 1) * 10 - 1;

        topLeft = new Point(left, top);
        topRight = new Point(right, top);
        bottomLeft = new Point(left, bottom);
        bottomRight = new Point(right, bottom);
    }
}
