/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package snakestatevisualiser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author Jeremy
 */
class SnakeStateFileLoader {

    SnakeStateFileLoader(File file, ObservableList<SnakeState> snakeStates) throws IOException, InvalidSnakeStateFormat {
//        snakeState = new ArrayList<SnakeState>();
        
        BufferedReader br = new BufferedReader(new FileReader(file));
        
        String stateString = "";
        int count = 0;
        String line;
        while ((line = br.readLine()) != null) {
           // process the line.
            stateString += line + '\n';
            count++;
            if (count%7 == 0)
            {
                try{
                    SnakeState state = new SnakeState(stateString);
                    snakeStates.add(state);
                    stateString = "";
                }catch (InvalidSnakeStateFormat e)
                {
                    System.out.println(stateString);
                    throw e;
                }
            }
        }
        if (count%7 != 0)
        {
            throw new InvalidSnakeStateFormat();
        }
        br.close();
    }

    /*ArrayList<SnakeState> getSnakeStates() {
        return snakeState;
    }*/
    
}
