/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakestatevisualiser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

/**
 *
 * @author Dean
 */
public class VisualiserController implements Initializable {

    private Label label;
    @FXML
    private Pane paneSnake;
    @FXML
    private ScrollBar scrollStateScroller;
    @FXML
    private TextField txtFile;
    @FXML
    private Button btnBrowse;
    @FXML
    private Button btnLoad;
    @FXML
    private TextArea txtAreaAdd;
    @FXML
    private Button btnAddState;
    @FXML
    private TextArea txtAreaCurrentState;
    @FXML
    private Button btnClearAll;

    private final ObjectProperty<SnakeState> currentState = new SimpleObjectProperty<>();
    private final ObservableList<SnakeState> obAllStates = FXCollections.observableArrayList();
    private final IntegerProperty numStates = new SimpleIntegerProperty(0);
    private final IntegerProperty currentStateNum = new SimpleIntegerProperty(0);

    @FXML
    private Label lblInvalid;
    @FXML
    private Label lblInvalidFile;
    @FXML
    private Label lblStateNum;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        paneSnake.getStyleClass().add("snakepane");
        scrollStateScroller.maxProperty().bind(Bindings.max(1, Bindings.size(obAllStates)));
        scrollStateScroller.minProperty().setValue(0);

        scrollStateScroller.setBlockIncrement(1);
        scrollStateScroller.setUnitIncrement(1);
        scrollStateScroller.setVisibleAmount(1);
        scrollStateScroller.disableProperty().bind(Bindings.lessThan(numStates, 2));

        numStates.bind(Bindings.size(obAllStates));

        IntegerBinding num = new IntegerBinding() {
            {
                bind(scrollStateScroller.valueProperty(), numStates);
            }

            @Override
            protected int computeValue() {
                int val = Math.min((int) Math.floor(scrollStateScroller.valueProperty().doubleValue()), numStates.getValue() - 1);
                return val;
            }
        };
        currentStateNum.bind(num);

        StringBinding b = new StringBinding() {
            {
                bind(numStates, currentStateNum);
            }

            @Override
            protected String computeValue() {
                if (numStates.getValue() == 0) {
                    return "No snake states currently.";
                }
                return "State " + (currentStateNum.getValue() + 1) + " of " + numStates.getValue();
            }
        };
        lblStateNum.textProperty().bind(b);
        lblInvalid.getStyleClass().add("error");
        lblInvalidFile.getStyleClass().add("error");

        /**
         * Keeps value in the range when things get removed
         */
        scrollStateScroller.maxProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            int val = Math.max(0, Math.min((int) scrollStateScroller.getValue(), obAllStates.size()));
            if (val != (int) scrollStateScroller.getValue()) {
                scrollStateScroller.setValue(val);
            }
        });

        /**
         * When the current state changes, it gets redrawn
         */
        currentState.addListener((ObservableValue<? extends SnakeState> observable, SnakeState oldValue, SnakeState newValue) -> {
            if (newValue == null) {
                txtAreaCurrentState.setText("");
                clearSnakes();
            } else {
                txtAreaCurrentState.setText(newValue.getStateString());
                drawSnakes(newValue);
            }
        });

        /**
         * Follows the scrollbar
         */
        currentStateNum.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (obAllStates.size() == 0) {
                    currentState.setValue(null);
                } else {
                    currentState.setValue(obAllStates.get(newValue.intValue()));
                }
            }
        });
    }

    public void drawSnakes(SnakeState state) {
        //jeremy
        paneSnake.getChildren().clear();
        if (state.snake1 != null) {
            paneSnake.getChildren().add(state.snake1);
        }
        if (state.snake2 != null) {
            paneSnake.getChildren().add(state.snake2);
        }
        if (state.snake3 != null) {
            paneSnake.getChildren().add(state.snake3);
        }
        if (state.snake4 != null) {
            paneSnake.getChildren().add(state.snake4);
        }
        if (state.apple1 != null) {
            paneSnake.getChildren().add(state.apple1);
        }
        if (state.apple2 != null) {
            paneSnake.getChildren().add(state.apple2);
        }
    }

    public void clearSnakes() {
        paneSnake.getChildren().clear();
    }

    @FXML
    private void browse(ActionEvent event) throws Exception {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        File file = fileChooser.showOpenDialog(null);
        
        if (file != null)
        {
            try{
                SnakeStateFileLoader loader = new SnakeStateFileLoader(file, obAllStates);
                lblInvalidFile.setText("");
            }catch (Exception e)
            {
                lblInvalidFile.setText("Invalid File");
            }            
        }
        
        
    }

    @FXML
    private void add(ActionEvent event) {
        //
        String stateString = txtAreaAdd.getText();
        try {
            SnakeState s = new SnakeState(stateString);
            obAllStates.add(s);
            txtAreaAdd.clear();
            lblInvalid.setText("");
            scrollStateScroller.setValue(scrollStateScroller.getMax());
        } catch (InvalidSnakeStateFormat e) {
            lblInvalid.setText("Invalid state string.");
        }
    }

    @FXML
    private void clear(ActionEvent event) {
        obAllStates.clear();
    }

}
