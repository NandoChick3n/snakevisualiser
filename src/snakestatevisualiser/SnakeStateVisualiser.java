/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakestatevisualiser;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Dean
 */
public class SnakeStateVisualiser extends Application {

    protected static final URL SNAKE_URL = SnakeStateVisualiser.class.
            getResource("snake.css");

    @Override
    public void start(Stage stage) throws Exception {
        stage.getIcons().add(new Image(SnakeStateVisualiser.class.getResourceAsStream("/images/arbok.png")));
        Parent root = FXMLLoader.load(getClass().getResource("Visualiser.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(SNAKE_URL.toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
